# Core-dns
## 1. Getting Started
We'll use port `5353` (_used for `avahi-daemon`, but is unnecessary since we have no idea what `mDNS` is_), because port `53` is sometime used for `dnsmasq` in some systems.
```sh
service avahi-daemon status
sudo service avahi-daemon stop
```

Bring them up:
```sh
docker-compose up -d && docker-compose logs -f
```

Try it out:
```sh
dig @localhost -p 5353 google.com
dig @localhost -p 5353 google.com +short
dig @127.0.0.1 -p 5353 google.com +noall +answer
```

Tada, nothing appears !!! :crocodile: !!!

Let's dig in asap...

## 2. References
- https://linux.die.net/man/1/dig

TL;DR:
```sh
dig @server name type
dig @server name type +short
dig @server name type +noall +answer
```

## 3. Work with CoreDNS
Reload when `Corefile` changes:
```sh
docker kill -s SIGUSR1 coredns
```
Note that above command is not neccessary anymore with the use of `reload` plugin.

Healthcheck:
```sh
curl 127.0.0.1:8080/health; echo
```

Prometheus metrics:
```sh
curl 127.0.0.1:9153/metrics
```

`CHAOS` query:
```sh
dig @127.0.0.1 -p 5353 CH TXT version.bind
dig @127.0.0.1 -p 5353 CH TXT version.server
dig @127.0.0.1 -p 5353 CH TXT authors.bind
dig @127.0.0.1 -p 5353 CH TXT hostname.bind
dig @127.0.0.1 -p 5353 CH TXT id.server
```

## 4. Fun facts

### 4.1. Reverse DNS lookup
| Domain       | Purpose                                   |
| ------------ | ----------------------------------------- |
| in-addr.arpa | Mapping of IPv4 addresses to domain names |

`arpa` was originally the acronym for the **Advanced Research Projects Agency** (_developed the `ARPANET`_), redefined as the backronym **Address and Routing Parameter Area**.

Sauce: https://en.wikipedia.org/wiki/.arpa

### 4.2. Zone file
For how to write [config/example.org](config/example.org), refer:
- https://en.wikipedia.org/wiki/Zone_file

